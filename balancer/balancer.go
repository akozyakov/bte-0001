package main

import (
	"bufio"
	"log"
	"net/http"
	"strconv"
	"sync/atomic"
)

type handler struct {
	port          string
	currentWorker uint32
	workers       []string
	workerURLs    []string
}

func (h *handler) initURLs() {
	h.workerURLs = make([]string, len(h.workers))
	for i, w := range h.workers {
		h.workerURLs[i] = "http://" + w + ":" + h.port
	}
}

func (h *handler) getNextWorker() string {
	currentWorker := atomic.AddUint32(&h.currentWorker, 1)
	currentWorker = currentWorker % uint32(len(h.workers))
	return h.workerURLs[currentWorker]
}

func (h *handler) handle(w http.ResponseWriter, r *http.Request) {
	log.Println("got", r.URL.String())
	retries := 0
	for {
		url := h.getNextWorker() + r.URL.String()

		q, err := http.NewRequest(r.Method, url, r.Body)
		if err != nil {
			log.Println("ERROR", "make request", url, err)
			break
		}

		resp, err := http.DefaultClient.Do(q)
		if err != nil {
			log.Println("ERROR", "send to worker", url, err)
			if retries++; retries >= len(h.workers)*2 {
				log.Println("ERROR", "send to all workers")
				return
			}
			continue
		}
		w.WriteHeader(resp.StatusCode)

		reader := bufio.NewReader(resp.Body)
		_, err = reader.WriteTo(w)
		if err != nil {
			log.Println("ERROR", "writing response body", url)
			return
		}

		log.Println("balanced to", url)
		break
	}
}

func startBalancer(port int, workers []string) error {
	log.Println("started")

	portStr := strconv.Itoa(port)
	h := &handler{workers: workers, port: portStr}
	h.initURLs()

	http.HandleFunc("/", h.handle)

	return http.ListenAndServe(":"+portStr, nil)
}
