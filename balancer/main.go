package main

import (
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
)

func prepareLog() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	log.SetPrefix("[BALANCER]")
}

func readWorkers() ([]string, error) {
	workers := os.Getenv("workers")
	if workers == "" {
		return []string{}, errors.New("no workers")
	}
	return strings.Split(workers, ","), nil
}

func readPort() int {
	portStr := os.Getenv("port")
	if port, err := strconv.Atoi(portStr); err != nil {
		return 1000
	} else {
		return port
	}
}

func onRecover() {
	if err := recover(); err != nil {
		log.Fatalln("ERROR", err)
	}
}

func main() {
	prepareLog()

	defer onRecover()

	port := readPort()

	workers, err := readWorkers()
	if err != nil {
		panic(err)
	}

	log.Println("workers", workers)

	err = startBalancer(port, workers)
	if err != nil {
		panic(err)
	}
}
