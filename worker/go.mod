module bitbucket.org/akozyakov/BTE-0001/worker

go 1.14

require (
	bitbucket.org/akozyakov/rabbit-wrapper v1.1.2
	github.com/pkg/errors v0.8.1 // indirect
	github.com/streadway/amqp v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.3.4
)
