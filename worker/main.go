package main

import (
	rw "bitbucket.org/akozyakov/rabbit-wrapper"
	"log"
	"net/http"
	"os"
	"strconv"
)

func prepareLog() {
	log.SetPrefix("[WORKER]")
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
}

type config struct {
	port                   int
	mongoConnectionString  string
	mongoDBName            string
	rabbitConnectionString string
	rabbitQueueName        string
}

func readConfig() config {
	result := config{port: 1000}

	port := os.Getenv("port")
	if port, err := strconv.Atoi(port); err == nil {
		result.port = port
	}

	result.mongoConnectionString = os.Getenv("mongo_connect")
	result.mongoDBName = os.Getenv("mongo_db")

	result.rabbitConnectionString = os.Getenv("rabbit_connect")
	result.rabbitQueueName = os.Getenv("rabbit_queue")

	return result
}

func main() {
	prepareLog()
	log.Println("started")

	config := readConfig()
	log.Println("config", config)

	mongo, err := newMongoDB(config.mongoConnectionString, config.mongoDBName, "test1")
	if err != nil {
		log.Fatalln("ERROR", err)
	}

	rabbit, err := rw.New(config.rabbitConnectionString, config.rabbitQueueName)
	if err != nil {
		log.Fatalln("ERROR", err)
	}

	handler := newHandler(mongo, rabbit)
	http.HandleFunc("/", handler.handle)

	log.Println("listen on", config.port)
	err = http.ListenAndServe(":"+strconv.Itoa(config.port), nil)

	log.Println("exit", err)
}
