package main

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"time"
)

type mongoDB struct {
	collection *mongo.Collection
}

func (db *mongoDB) save(data map[string]interface{}) (interface{}, error) {
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	res, err := db.collection.InsertOne(ctx, bson.M(data))
	if err != nil {
		return nil, err
	}

	id := res.InsertedID
	return id, nil
}

func newMongoDB(connectionString, dbName, collectionName string) (*mongoDB, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(connectionString))
	if err != nil {
		return nil, err
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}

	collection := client.Database(dbName).Collection(collectionName)
	result := &mongoDB{collection: collection}

	return result, nil
}
