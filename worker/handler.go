package main

import (
	rw "bitbucket.org/akozyakov/rabbit-wrapper"
	"bytes"
	"encoding/json"
	"log"
	"net/http"
)

type handler struct {
	db     *mongoDB
	rabbit rw.Sender
}

func (h *handler) handle(rw http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	log.Println("got", r.URL.String())
	rw.WriteHeader(http.StatusOK)

	buffer := &bytes.Buffer{}
	_, err := buffer.ReadFrom(r.Body)
	if err != nil {
		log.Println("ERROR", "read body", err)
		return
	}

	body := make(map[string]interface{})
	err = json.NewDecoder(buffer).Decode(&body)
	if err != nil {
		log.Println("ERROR", "parse body", err)
		return
	}

	id, err := h.db.save(body)
	if err != nil {
		log.Println("ERROR", "save to mongo", err)
		return
	}

	log.Println("new id", id)

	_, err = rw.Write([]byte("ok"))
	if err != nil {
		log.Println("ERROR", "write body", err)
		return
	}

	body["id"] = id
	msg, err := json.Marshal(body)
	if err != nil {
		log.Println("ERROR", "marshal body to json", err)
		return
	}

	err = h.rabbit.Send(msg)
	if err != nil {
		log.Println("ERROR", "send to rabbit", err)
		return
	}

	log.Println("done", r.URL.String())
}

func newHandler(db *mongoDB, rabbit rw.Sender) *handler {
	return &handler{db: db, rabbit: rabbit}
}
