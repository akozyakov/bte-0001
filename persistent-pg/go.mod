module bitbucket.org/akozyakov/bte-0001/persistent-pg

go 1.14

require (
	bitbucket.org/akozyakov/rabbit-wrapper v1.1.2
	github.com/jackc/pgx/v4 v4.6.0
	github.com/streadway/amqp v1.0.0
)
