package main

import (
	rw "bitbucket.org/akozyakov/rabbit-wrapper"
	"context"
	"encoding/json"
	pool "github.com/jackc/pgx/v4/pgxpool"
	"log"
)

const workersCount = 1000

func handleMessages(receiver rw.Receiver, pg *pool.Pool) error {
	ch, err := receiver.Receive()
	if err != nil {
		return err
	}

	for i := 0; i < workersCount; i++ {
		go func() {
			for delivery := range ch {
				messageJSON := delivery.Body
				data := make(map[string]interface{})

				err := json.Unmarshal(messageJSON, &data)
				if err != nil {
					log.Println("ERROR", "unmarshal message", err, messageJSON)
					continue
				}

				t, err := pg.Exec(
					context.Background(),
					"insert into random_data(id, data) values ($1, $2)",
					data["id"],
					data,
				)
				if err != nil {
					log.Println("ERROR", "pg exec", err)
					continue
				}

				if t.RowsAffected() != 1 {
					log.Println("ERROR", "rows affected != 1", t.RowsAffected())
					continue
				}

				log.Println("message saved", data["id"])
			}
		}()
	}

	done := make(<-chan bool)
	<-done

	return nil
}
