package main

import (
	rw "bitbucket.org/akozyakov/rabbit-wrapper"
	"context"
	"fmt"
	pool "github.com/jackc/pgx/v4/pgxpool"
	"log"
	"os"
)

type config struct {
	connectionStringPG     string
	connectionStringRabbit string
	queueNameRabbit        string
}

func prepareLogger() {
	log.SetPrefix("[PERSISTENT-PG]")
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
}

func readConfig() (*config, error) {
	result := &config{}
	result.connectionStringPG = os.Getenv("connection_string_pg")
	result.connectionStringRabbit = os.Getenv("connection_string_rabbit")
	result.queueNameRabbit = os.Getenv("queue_name_rabbit")

	if result.connectionStringPG == "" || result.connectionStringRabbit == "" || result.queueNameRabbit == "" {
		return nil, fmt.Errorf("connection empty in env vars: %v", result)
	}

	return result, nil
}

func main() {
	prepareLogger()

	config, err := readConfig()
	if err != nil {
		log.Fatal("ERROR", "read config", err)
	}
	log.Println("config", config)

	rabbit, err := rw.New(config.connectionStringRabbit, config.queueNameRabbit)
	if err != nil {
		log.Fatal("ERROR", "connect to rabbit", err)
	}

	pg, err := pool.Connect(context.Background(), config.connectionStringPG)
	if err != nil {
		log.Fatal("ERROR", "connect to postgres", err)
	}

	log.Fatal("FINISH", handleMessages(rabbit, pg))
}
