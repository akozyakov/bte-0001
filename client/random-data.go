package main

import "math/rand"

type commonData map[string]interface{}

func makeRandomString(start rune, len, max int32) string {
	buffer := make([]byte, len)
	for i := range buffer {
		buffer[i] = byte(int32(start) + rand.Int31n(max))
	}
	return string(buffer)
}

func makeRandomData(recursive bool) commonData {
	cnt := rand.Int31n(3) + 2
	result := make(commonData, cnt)

	for i := int32(0); i < cnt; i++ {
		key := makeRandomString('a', rand.Int31n(10)+2, 26)
		var val interface{} = makeRandomString('a', rand.Int31n(10)+2, 26)
		if recursive && rand.Int31n(100) < 25 {
			val = makeRandomData(false)
		}
		result[key] = val
	}

	return result
}
