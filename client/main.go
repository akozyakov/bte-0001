package main

import (
	"context"
	"golang.org/x/time/rate"
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func parseEnvVars() (string, int, int) {
	endpoint := os.Getenv("client_endpoint")

	totalRequestsStr := os.Getenv("client_total_requests")
	totalRequests, err := strconv.Atoi(totalRequestsStr)
	if err != nil {
		totalRequests = 1000
	}

	maxRequestsPerSecStr := os.Getenv("client_requests_per_sec")
	maxRequestsPerSec, err := strconv.Atoi(maxRequestsPerSecStr)
	if err != nil {
		maxRequestsPerSec = 100
	}

	return endpoint, totalRequests, maxRequestsPerSec
}

func throttle(t task, totalRequests, maxRequestsPerSec int) {
	ctx := context.Background()
	lim := rate.NewLimiter(rate.Limit(maxRequestsPerSec), maxRequestsPerSec)
	for i := 0; i < totalRequests; i++ {
		lim.Wait(ctx)
		t.run()
	}
}

func prepareLog() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	log.SetPrefix("[CLIENT]")
}

func seedRandom() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	prepareLog()
	seedRandom()
	endpoint, totalRequests, maxRequestsPerSec := parseEnvVars()
	client := newClientTask(endpoint)
	log.Println("started on", endpoint)

	time.Sleep(10 * time.Second)
	throttle(client, totalRequests, maxRequestsPerSec)
}
