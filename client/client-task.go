package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
)

type clientTask struct {
	endpoint string
}

type task interface {
	run()
}

func (t *clientTask) run() {
	log.Println("sending")
	json, _ := json.Marshal(makeRandomData(true))
	resp, err := http.DefaultClient.Post(t.endpoint, "application/json", bytes.NewBuffer(json))
	if err != nil {
		log.Println("ERROR", err)
		return
	}
	defer resp.Body.Close()
}

func newClientTask(endpoint string) *clientTask {
	return &clientTask{endpoint: endpoint}
}
